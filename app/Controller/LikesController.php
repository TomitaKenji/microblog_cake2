<?php
App::uses('AppController', 'Controller');

class LikesController extends AppController
{
  public function like()
  {
    $this->loadModel('Post');
    $this->autoRender = false;
    $this->autoLayout = false;
    if ($this->request->is('ajax')) {
      $cnt = $this->Like->find('count',
      [
        'conditions' => [
        'Like.user_id'=> $this->Auth->user('id'),
        'post_id' => $this->data['name']
        ]
      ]);
      $count = $this->Like->find('count',
      [
        'conditions' => [
        'post_id' => $this->data['name']
        ]
      ]);
      $count++;
      $like =
      [
        'Like' => [
        'user_id' => $this->Auth->user('id'),
        'post_id' => $this->data['name']
        ]
      ];
      if($cnt == 0 ){
        $this->Like->save($like);
        $this->Post->id = $this->data['name'];
        $this->Post->saveField('like_count',$count);
        return $count;
        } else {
        $this->Like->deleteAll(['user_id'=> $this->Auth->user('id'),
        'post_id' => $this->data['name']
       ],
        false);
        $this->Post->id = $this->data['name'];
        $this->Post->saveField('like_count',$count-2);
        return $count-2;
      }
    }
  }
}
