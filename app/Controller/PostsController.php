<?php

class PostsController extends AppController {
    public $helpers = array('Html', 'Form');

    public $components = array('Paginator', 'Session','Flash');

    public function index()
    {
        $this->loadModel('User');
        $id = $this->Auth->user('id');
        try {
        $paginate['fields'][] = '*';
        $paginate['joins'][] = [
          'type' => 'LEFT',
          'table' => 'users',
          'alias' => 'User',
          'conditions' => 'User.id = Post.user_id'
        ];
        $paginate['joins'][] = [
          'type' => 'LEFT',
          'table' => 'posts',
          'alias' => 'Share',
          'conditions' => 'Share.id = Post.shared_id'
       ];
       $paginate['joins'][] = [
         'type' => 'LEFT',
         'table' => 'users',
         'alias' => 'ShareUser',
         'conditions' => 'ShareUser.id = Share.user_id'
      ];
       $paginate['joins'][] = [
         'type' => 'LEFT',
         'table' => 'followers',
         'alias' => 'Follower',
         'conditions' => 'User.id = Follower.followed_id'
      ];
        $paginate['limit'] = 10;
        $paginate['order']['Post.id'] = "desc";
        $paginate['conditions'] = [
         'Follower.user_id' => $id,
         'Post.deleted' => null
      ];
        $paginate['contain'] = [
          'Like' => [
            'conditions' => [
            'Like.user_id =' => $id
          ]
        ]
      ];

      $this->Paginator->settings = $paginate;
      $posts = $this->Paginator->paginate('Post');
      $this->set('posts', $posts);
      $this->set('users', $this->User->find('first',
       [
            'conditions' => [
            'User.id' => $id
            ]
          ]
        )
      );
      } catch (Exception $e) {
        $this->Paginator->settings = $paginate;
        $this->request->params['named']['page'] = 1;
        $result = $this->Paginator->paginate('Post');
        $this->set('posts', $result);
        $this->set('users', $this->User->find('first',
        [
             'conditions' => [
             'User.id' => $id
             ]
           ]
         )
       );
    }
  }

    public function view($id = null) {
      $this->Post->id = $id;
      if (!$this->Post->exists()) {
       $this->Session->setFlash('unexist Post...','default', array('class' => 'alert-danger'));
       $this->redirect(array('controller'=>'posts','action'=>'index'));
      }

      if ( 1 == $this->Post->find('count',['conditions' => ['Post.deleted' => '1','Post.id =' => $id]])) {
       $this->Session->setFlash('Already deleted Post...','default', array('class' => 'alert-danger'));
       $this->redirect(array('controller'=>'posts','action'=>'index'));
       }

        $this->loadModel('User');
        $this->loadModel('Comment');
        $this->Post->id = $id;
        $this->set('post', $this->Post->read());
        $option =  [
            'fields' => '*',
            'order' => 'Post.id DESC',
            'joins' => [
                [
                'type' => 'LEFT',
                'table' => 'users',
                'alias' => 'CommentUser',
                'conditions' => 'CommentUser.id = Comment.user_id'
                ]
             ]
          ];
          $option['conditions'] = ['Comment.post_id' => $id];
          $this->set('comments',$this->Comment->find('all',$option));
        $this->set('users', $this->User->find('first', [
          'conditions' => ['User.id' => $this->Auth->user('id')]
        ]
      ));
    }

    public function add()
    {
        if ($this->request->is('post')) {
          if ($this->data['Post']['image_name']['name'] == null) {
            $post = [
              'Post' => [
                'user_id' => $this->Auth->user('id'),
                'post' => $this->data['Post']['post']

              ]
            ];
            if ($this->Post->save($post)) {
            $this->Session->setFlash('Success!', 'default', array('class' => 'alert-success'));
            $this->redirect(array('action'=>'index'));
            } else {
              $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
              $this->redirect(array('action'=>'index'));
            }

          } else {
            $this->Post->set($this->request->data);

             if ($this->Post->validates(array('fieldList' => array('image_name')))) {
               $file = $this->request->data['Post']['image_name'];
               $newimage = uniqid().$file['name'];
                if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
                  $this->Post->create();
                  $post = [
                    'Post' => [
                      'user_id' => $this->Auth->user('id'),
                      'post' => $this->data['Post']['post'],
                      'image_name' => $newimage
                    ]
                  ];
                  //save
                  if( $this->Post->save($post)) {
                    $this->Session->setFlash('Success', 'default', array('class' => 'alert-success'));
                    $this->redirect(array('action'=>'index'));
                  // saveエラーの場合
                  } else {
                    $this->Session->setFlash('should have post!', 'default', array('class' => 'alert-danger'));
                    $this->redirect(array('action'=>'index'));
                  }
                } else {
                  // ファイル移動エラーの場合
                    $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
                    $this->redirect(array('action'=>'index'));
                }
             // バリデーションNGの場合
             } else {
               if (isset($this->Post->validationErrors['post']['0'])) {
                  $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
                }
               if (isset($this->Post->validationErrors['image_name']['0'])) {
                 $this->Session->setFlash($this->Post->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
               }
              $this->redirect(array('action'=>'index'));
             }
           }
        }
    }

    public function share($id = null)
    {
        if ($this->request->is('post')) {

          if ($this->data['Post']['image_name']['name'] == null) {
            $post = [
              'Post' => [
                'user_id' => $this->Auth->user('id'),
                'post' => $this->data['Post']['post'],
                'shared_id'=> $this->data['Post']['id']
              ]
            ];
            if ($this->Post->save($post)) {
            $this->Session->setFlash('Success!', 'default', array('class' => 'alert-success'));
            $this->redirect(array('action'=>'index'));
            } else {
              $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
              $this->redirect(array('action'=>'index'));
            }

          } else {
            $this->Post->set($this->request->data);
             if ($this->Post->validates(array('fieldList' => array('image_name')))) {
               $file = $this->request->data['Post']['image_name'];
               $newimage = uniqid().$file['name'];
                if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
                  $this->Post->create();
                  $post = [
                    'Post' => [
                      'user_id' => $this->Auth->user('id'),
                      'post' => $this->data['Post']['post'],
                      'image_name' => $newimage,
                      'shared_id'=> $this->data['Post']['id']
                    ]
                  ];
                  //save
                  if( $this->Post->save($post)) {
                    $this->Session->setFlash('Success', 'default', array('class' => 'alert-success'));
                    $this->redirect(array('action'=>'index'));
                  // saveエラーの場合
                  } else {
                    $this->Session->setFlash('should have post!', 'default', array('class' => 'alert-danger'));
                    $this->redirect(array('action'=>'index'));
                  }
                } else {
                  // ファイル移動エラーの場合
                    $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
                    $this->redirect(array('action'=>'index'));
                }
             // バリデーションNGの場合
             } else {
               if (isset($this->Post->validationErrors['post']['0'])) {
                  $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
                }
               if (isset($this->Post->validationErrors['image_name']['0'])) {
                 $this->Session->setFlash($this->Post->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
               }
              $this->redirect(array('action'=>'index'));
             }
           }
         }
       }

    public function edit($id = null)
    {
      $option['conditions'] = ['Post.id' => $this->data['Post']['id'],
      'Post.user_id' => $this->Auth->user('id')
      ];

      $result = $this->Post->find('count', $option);
       if($result != 1) {
         $this->Session->setFlash('can edit own post!','default', array('class' => 'alert-danger'));
         $this->redirect(array('action'=>'index'));
       }

       if ($this->data['Post']['image_name']['name'] != null) {
       $file = $this->request->data['Post']['image_name'];
       $newimage = uniqid().$file['name'];
        if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
           $this->Post->set($this->request->data);
        if ($this->Post->validates()) {
           $this->Post->create();
           $data = array('Post' => array(
             'id' => $this->data['Post']['id'],
             'post' => $this->data['Post']['post'],
             'image_name' => $newimage
             )
           );
       // 更新する項目（フィールド指定）
       $fields = array('post','image_name');
       // 更新
        if($this->Post->save($data, false, $fields)) {
          $this->Session->setFlash('Edit!', 'default', array('class' => 'alert-success'));
          $this->redirect(array('action'=>'index'));
        } else {
          $this->Session->setFlash('Please, enter something!', 'default', array('class' => 'alert-danger'));
          $this->redirect(array('action'=>'index'));
        }

      } else {
        if (isset($this->Post->validationErrors['post']['0'])) {
        $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
        }
        if (isset($this->Post->validationErrors['image_name']['0'])) {
        $this->Session->setFlash($this->Post->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
        }

        $this->redirect(array('action'=>'index'));
      }
    } else {
      $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
      $this->redirect(array('action'=>'index'));
    }

  } else {
    $this->Post->set($this->request->data);
    // exit;
    if ($this->Post->validates(array('fieldList' => array('post')))) {
       $this->Post->create();
       $data = array('Post' => array(
         'id' => $this->data['Post']['id'],
         'post' => $this->data['Post']['post']
         )
       );
       $fields = array('post');
       if($this->Post->save($data, false, $fields)) {
         $this->Session->setFlash('Edit!', 'default', array('class' => 'alert-success'));
         $this->redirect(array('action'=>'index'));
       } else {
         $this->Session->setFlash('Please, enter something!', 'default', array('class' => 'alert-danger'));
         $this->redirect(array('action'=>'index'));
       }
     } else {
       if (isset($this->Post->validationErrors['post'])) {
       $this->Session->setFlash($this->Post->validationErrors['post']['0'],'default', array('class' => 'alert-danger'));
       $this->redirect(array('action'=>'index'));
       }
     }
   }
 }

    public function delete($id)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $option['conditions'] = ['Post.id' => $id,
        'Post.user_id' => $this->Auth->user('id')
        ];
        $this->Post->id = $id;

        if (1 == $this->Post->find('count',$option)) {
          $this->Post->saveField('deleted','1');
          $this->Session->setFlash('Deleted!','default', array('class' => 'alert-danger'));
          $this->redirect(array('action'=>'index'));
        } else {
          $this->Session->setFlash('can not delete...','default', array('class' => 'alert-danger'));
          $this->redirect(array('action'=>'index'));
        }
    }

    public function search()
    {
      if (isset($this->request->query['search'])){
        $keyword = $this->request->query['search'];
        $this->Session->write('search', $keyword);
      }
      $this->loadModel('User');
      $id = $this->Auth->user('id');
      $this->set('me', $this->User->find('first', [
        'conditions' => ['User.id' => $this->Auth->user('id')]
      ]
      ));

      $this->request->query['search'] = $this->Session->read('search');

      $keyword = $this->Session->read('search');
      $keyword = '%'.$keyword.'%';
      try {
      $paginate['fields'][] = '*';
      $paginate['joins'][] = [
        'type' => 'LEFT',
        'table' => 'users',
        'alias' => 'User',
        'conditions' => 'User.id = Post.user_id'
      ];
      $paginate['joins'][] = [
        'type' => 'LEFT',
        'table' => 'posts',
        'alias' => 'Share',
        'conditions' => 'Share.id = Post.shared_id'
     ];
     $paginate['joins'][] = [
       'type' => 'LEFT',
       'table' => 'users',
       'alias' => 'ShareUser',
       'conditions' => 'ShareUser.id = Share.user_id'
    ];
      $paginate['limit'] = 10;
      $paginate['order']['Post.id'] = "desc";
      $paginate['conditions'] = [
       'Post.deleted' => null,
       'Post.post LIKE' => $keyword
    ];
      $paginate['contain'] = [
        'Like' => [
          'conditions' => [
          'Like.user_id =' => $id
        ]
      ]
    ];
    $this->request->query;
    $this->Paginator->settings = $paginate;
    $result = $this->Paginator->paginate('Post');
    $this->set('result', $result);
    $this->set('count', $this->Post->find('count', $paginate));
    $this->set('keyword',$this->request->query['search']);
    } catch (Exception $e) {
     $this->request->params['named']['page'] = 1;
     $result = $this->Paginator->paginate('Post');
     $this->set('result', $result);
     $this->set('count', $this->Post->find('count', $paginate));
    }
  }
}
