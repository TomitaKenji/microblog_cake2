<?php

class CommentsController extends AppController {
    public $helpers = array('Html', 'Form');

    public function add()
    {
      // $this->loadModel('Post');
      if ($this->request->is('post')) {
        if ($this->data['Comment']['image_name']['name'] == null) {
        $this->Comment->set($this->request->data);
         if ($this->Comment->validates(array('fieldList' => array('comment')))) {
          $this->Comment->create();
          $comment = [
                   'Comment' => [
                   'user_id' => $this->Auth->user('id'),
                   'post_id' => $this->data['Comment']['post_id'],
                   'comment' => $this->data['Comment']['comment']
                 ]
               ];
          if ($this->Comment->save($comment)) {
          $this->Session->setFlash('Success!', 'default', array('class' => 'alert-success'));
          $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
          } else {
            // var_dump($comment);
            // exit;
            $this->Session->setFlash('Please, one more','default', array('class' => 'alert-danger'));
            $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
          }
          // var_dump($comment);
          // exit;
        } else {
          $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
          $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));

        }

        } else {
          $this->Comment->set($this->request->data);
           if ($this->Comment->validates()) {
             $file = $this->request->data['Comment']['image_name'];
             $newimage = uniqid().$file['name'];
              if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
                $this->Comment->create();
                $comment = [
                  'Comment' => [
                    'user_id' => $this->Auth->user('id'),
                    'post_id' => $this->data['Comment']['post_id'],
                    'comment' => $this->data['Comment']['comment'],
                    'image_name' => $newimage
                    // 'shared_id'=> $this->data['Post']['id']
                  ]
                ];
                //save
                if( $this->Comment->save($comment)) {
                  $this->Session->setFlash('Success', 'default', array('class' => 'alert-success'));
                  $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
                // saveエラーの場合
                } else {
                  $this->Session->setFlash('Failure!', 'default', array('class' => 'alert-success'));
                  $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
                }
              } else {
                // ファイル移動エラーの場合
                  $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
                  $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
              }
           // バリデーションNGの場合
           } else {
             if (isset($this->Comment->validationErrors['comment']['0'])) {
                $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
              }
             if (isset($this->Comment->validationErrors['image_name']['0'])) {
               $this->Session->setFlash($this->Comment->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
             }
            $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
           }
         }
      }
        // if ($this->request->is('post')) {
        //     if ($this->data['Comment']['image_name']['name'] == null) {
        //       $comment = [
        //           'Comment' => [
        //           'user_id' => $this->Auth->user('id'),
        //           'post_id' => $this->data['Comment']['post_id'],
        //           'comment' => $this->data['Comment']['comment']
        //         ]
        //       ];
        //       if ($this->Comment->save($comment)) {
        //       $this->Session->setFlash('Success!', 'default', array('class' => 'alert-success'));
        //       $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //       } else {
        //         $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
        //         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //       }
        //
        //     } else {
        //       $this->Comment->set($this->request->data);
        //        if ($this->Comment->validates(array('fieldList' => array('image_name')))) {
        //          $file = $this->request->data['Comment']['image_name'];
        //          $newimage = uniqid().$file['name'];
        //           if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
        //             $this->Comment->create();
        //             $comment = [
        //               'Comment' => [
        //                 'user_id' => $this->Auth->user('id'),
        //                 'post_id' => $this->data['Comment']['post_id'],
        //                 'comment' => $this->data['Comment']['comment'],
        //                 'image_name' => $newimage
        //                 // 'shared_id'=> $this->data['Post']['id']
        //               ]
        //             ];
        //             //save
        //             if($this->Comment->save($comment)) {
        //               $this->Session->setFlash('Success', 'default', array('class' => 'alert-success'));
        //               $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //             // saveエラーの場合
        //             } else {
        //               $this->Session->setFlash('Failure!', 'default', array('class' => 'alert-success'));
        //               $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //             }
        //           } else {
        //             // ファイル移動エラーの場合
        //               $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
        //               $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //           }
        //        // バリデーションNGの場合
        //        } else {
        //          if (isset($this->Comment->validationErrors['comment'])) {
        //             $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
        //           }
        //          if (isset($this->Comment->validationErrors['image_name'])) {
        //            $this->Session->setFlash($this->Comment->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
        //          }
        //         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        //        }
        //      }
        //   }
        }

    public function edit($id = null)
    {
      $option['conditions'] = ['Comment.id' => $this->data['Comment']['id'],
      'Comment.user_id' => $this->Auth->user('id')
      ];

      $result = $this->Comment->find('count', $option);
       if($result != 1) {
         $this->Session->setFlash('can edit own comment!','default', array('class' => 'alert-danger'));
         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
       }


       // $this->Comment->set($this->request->data);
       //  if ($this->Comment->validates(array('fieldList' => array('image_name')))) {
       //   var_dump($this->Comment->validationErrors['image_name']);
       //   var_dump($this->request->data);
       //   exit;
       // } else {
       //   var_dump($this->Comment->validationErrors['image_name']);
       //   exit;
       // }
     //   $this->Comment->create();
     //   $this->Comment->set($this->request->data);
     //   if ($this->Comment->validates()) {
     //      $this->Comment->create();
     //      $data = array('Comment' => array(
     //        'id' => $this->data['Comment']['id'],
     //        'comment' => $this->data['Comment']['comment'],
     //        'image_name' => $newimage
     //        )
     //      );
     //  // 更新する項目（フィールド指定）
     //  $fields = array('comment','image_name');
     //  // 更新
     //   if($this->Comment->save($data, false, $fields)) {
     //     $this->Session->setFlash('Edit!', 'default', array('class' => 'alert-success'));
     //     $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
     //   }
     // } else {
     //   if (isset($this->Comment->validationErrors['comment']['0'])) {
     //   $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
     //   }
     //   if (isset($this->Comment->validationErrors['image_name']['0'])) {
     //   $this->Session->setFlash($this->Comment->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
     //   }
     //   $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
     // }
     // exit;

       if ($this->data['Comment']['image_name']['name'] != null) {
       $file = $this->request->data['Comment']['image_name'];
       $newimage = uniqid().$file['name'];
        if ( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage)) {
        $this->Comment->set($this->request->data);
        if ($this->Comment->validates()) {
           $this->Comment->create();
           $data = array('Comment' => array(
             'id' => $this->data['Comment']['id'],
             'comment' => $this->data['Comment']['comment'],
             'image_name' => $newimage
             )
           );
       // 更新する項目（フィールド指定）
       $fields = array('comment','image_name');
       // 更新
        if($this->Comment->save($data, false, $fields)) {
          $this->Session->setFlash('Edit!', 'default', array('class' => 'alert-success'));
          $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
        }
      } else {
        if (isset($this->Comment->validationErrors['comment']['0'])) {
        $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
        }
        if (isset($this->Comment->validationErrors['image_name']['0'])) {
        $this->Session->setFlash($this->Comment->validationErrors['image_name']['0'],'default', array('class' => 'alert-danger'));
        }
        $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
      }
    } else {
      $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
      $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
    }

  } else {
    $this->Comment->set($this->request->data);
    if ($this->Comment->validates(array('fieldList' => array('comment')))) {
       $this->Comment->create();
       $data = array('Comment' => array(
         'id' => $this->data['Comment']['id'],
         'comment' => $this->data['Comment']['comment']
         )
       );
       $fields = array('comment');
       if($this->Comment->save($data, false, $fields)) {
         $this->Session->setFlash('Edit!', 'default', array('class' => 'alert-success'));
         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
       }
         $this->Session->setFlash('Failure', 'default', array('class' => 'alert-success'));
         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
       } else {
         if (isset($this->Comment->validationErrors['comment'])) {
         $this->Session->setFlash($this->Comment->validationErrors['comment']['0'],'default', array('class' => 'alert-danger'));
         $this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
         }
       }

     }
    }

    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Comment->delete($id)) {
            $this->Session->setFlash('Deleted!','default', array('class' => 'alert-danger'));
            $this->redirect(array('controller'=>'posts'));
        } else {
            $this->Session->setFlash('failed!','default', array('class' => 'alert-danger'));
        }
    }
}
