<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $components = array('Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array('signup', 'activate', 'login'));
    }

    public function signup()
    {
        if ($this->request->is('post')) {
            if ($this->User->save($this->data)) {
                // 本登録用のリンクを作成
                $url = 'activate/' . $this->User->id . '/' . $this->User->getActivationHash();
                $url = Router::url($url, true);
                //--------------------------------------------------------------
                $username = $this->data['User']['username'];
                $mainbody = [
                   'name' => "Dear  ".$this->data['User']['first_name']." ".$this->data['User']['last_name'],
                   'content' =>"Your Account has been registered successfully\nUsername:".$username."\nActivate your account by clicking on the below URL\n\nURL:".$url
                 ];

                // 本登録の案内メールを送信
                $email = new CakeEmail('gmail');
                $email->from( array('tora169mm@gmail.com' => 'Sender'));
                $email->to($this->data['User']['email']);
                $email->subject('Account Activation Link send on your Email');
                $email->send($mainbody); // メール本文に本登録用リンクを記す
                //--------------------------------------------------------------
                $this->Session->setFlash('Please confirm your email address to complete your registration','default', array('class' => 'alert-success'));
            } else {
                $this->Session->setFlash('Please enter once more','default', array('class' => 'alert-danger'));
            }
        }
    }

    public function activate($user_id = null, $in_hash = null)
    {
        $this->User->id = $user_id;
        if ($this->User->exists() && $in_hash == $this->User->getActivationHash()) {
            $this->User->saveField('active', 1);
            $this->loadModel('Follower');
            $follow = [
              'Follower' => [
              'user_id' => $user_id,
              'followed_id' => $user_id
              ]
            ];
            $this->Follower->save($follow);
            $this->Session->setFlash('Successful Registration','default', array('class' => 'alert-success'));
        } else {
            $this->Session->setFlash('Wrong Link','default', array('class' => 'alert-danger'));
        }
    }

    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->User->id = $this->Auth->user('id');
                return $this->redirect($this->Auth->redirect(array('controller'=>'posts','action'=>'index')));
            } else {
                $active = $this->User->field('active', array('username' => $this->data['User']['username']));
                if ($active === 0) {
                    $this->Session->setFlash('Please, Check My Email', 'default', array('class' => 'alert-success'));
                } else {
                  if ($this->data['User']['username'] == null || $this->data['User']['password'] == null ){
                      $this->Session->setFlash('Please, Enter something on Username/Password','default', array('class' => 'alert-danger'));
                   } else {
                      $this->Session->setFlash('The username and password you entered did not match my records.','default', array('class' => 'alert-danger'));
                   }
                }
            }
        }
    }

    public function password()
    {
          $id = $this->Auth->user('id');
          $this->User->id = $id;
          if (!$this->User->exists()) {
              throw new NotFoundException('invalid User');
          }
          if ($this->request->is('post') || $this->request->is('put')) {
              if ($this->User->save($this->request->data)) {
                  $this->Session->setFlash('Update Password','default', array('class' => 'alert-success'));
                  $this->redirect(array('controller' => 'posts', 'action' => 'index'));
              } else {
                  $this->Session->setFlash('can not Update Password','default', array('class' => 'alert-danger'));
              }
          }
          $this->request->data = $this->User->read(null, $id);
      }

    public function edit()
    {
          $id = $this->Auth->user('id');
          $this->User->id = $id;
          $this->set('me', $this->User->find('first', [
           'conditions' => ['User.id' => $this->Auth->user('id')]
               ]
             )
           );
          if (!$this->User->exists()) {
              throw new NotFoundException('Invalid User');
          }
          if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->data['User']['image_name']['name'] != null) {
              $file = $this->request->data['User']['image_name'];
              $newimage = uniqid().$file['name'];
              $user = [
              'User' => [
              'first_name' => $this->data['User']['first_name'],
              'last_name' => $this->data['User']['last_name'],
              'username' => $this->data['User']['username'],
              'image_name' => $newimage
              ]
            ];

            if( move_uploaded_file($file['tmp_name'],'../webroot/img/'.$newimage))
            {
              if ($this->User->save($user)) {
                $this->Session->setFlash('Success', 'default', array('class' => 'alert-success'));
                $this->redirect(array('controller'=>'posts','action'=>'index'));

                } else {
                  $this->render();
                  if (isset($this->User->validationErrors['first_name'])){
                    $this->Session->setFlash('First name: '.$this->User->validationErrors['first_name']['0'],'default', array('class' => 'alert-dangers'));
                  }
                  if (isset($this->User->validationErrors['last_name'])){
                    $this->Session->setFlash('Last name: '.$this->User->validationErrors['last_name']['0'],'default', array('class' => 'alert-dangers'));
                  }
                  if (isset($this->User->validationErrors['username'])){
                    $this->Session->setFlash('Username: '.$this->User->validationErrors['username']['0'],'default', array('class' => 'alert-dangers'));
                  }
                  if (isset($this->User->validationErrors['image_name'])){
                    $this->Session->setFlash('Picture: '.$this->User->validationErrors['image_name']['0'],'default', array('class' => 'alert-dangers'));
                  }
                $this->redirect(array('action'=>'edit'));
                }

              } else {
                $this->Session->setFlash('Error uploading file','default', array('class' => 'alert-danger'));
                $this->redirect(array('action'=>'edit'));
              }
    // 　　　　 画像がなければ
            } else {
                $user = [
                  'User' => [
                  'first_name' => $this->data['User']['first_name'],
                  'last_name' => $this->data['User']['last_name'],
                  'username' => $this->data['User']['username']
                  ]
                ];
                if ($this->User->save($user)) {
                    $this->Session->setFlash('Success!','default', array('class' => 'alert-success'));
                    $this->redirect(array('controller'=>'posts','action'=>'index'));
                } else {
                if (isset($this->User->validationErrors['first_name'])){
                  $this->Session->setFlash('First name: '.$this->User->validationErrors['first_name']['0'],'default', array('class' => 'alert-dangers'));
                }
                if (isset($this->User->validationErrors['last_name'])){
                  $this->Session->setFlash('Last name: '.$this->User->validationErrors['last_name']['0'],'default', array('class' => 'alert-dangers'));
                }
                if (isset($this->User->validationErrors['username'])){
                  $this->Session->setFlash('Username: '.$this->User->validationErrors['username']['0'],'default', array('class' => 'alert-dangers'));
                }

                 $this->redirect(array('action'=>'edit'));
                }
             }
          }
          $this->request->data = $this->User->read(null, $id);
      }

      public function profile($id = null)
      {
        $this->User->id = $id;
        if (!$this->User->exists()) {
         $this->Session->setFlash('Unexist User','default', array('class' => 'alert-danger'));
         $this->redirect(array('controller'=>'posts','action'=>'index'));
        }
        $this->loadModel('Post');
        $this->loadModel('Follower');
        $this->loadModel('Like');
        // そのユーザのポスト
        $option['fields'][] = '*';
        $option['joins'][] = [
          'type' => 'LEFT',
          'table' => 'users',
          'alias' => 'User',
          'conditions' => 'User.id = Post.user_id'
        ];
        $option['joins'][] = [
          'type' => 'LEFT',
          'table' => 'posts',
          'alias' => 'Share',
          'conditions' => 'Share.id = Post.shared_id'
       ];
       $option['joins'][] = [
         'type' => 'LEFT',
         'table' => 'users',
         'alias' => 'ShareUser',
         'conditions' => 'ShareUser.id = Share.user_id'
      ];
        $option['order']['Post.id'] = "desc";
        $option['conditions'] = [
         'User.id' => $id,
         'Post.deleted' => null
      ];
        $option['contain'] = [
          'Like' => [
            'conditions' => [
            'Like.user_id =' => $this->Auth->user('id')
          ]
        ]
      ];
        // number of following
        $this->set('following', $this->Follower->find('count',[
          'conditions' => ['Follower.user_id' => $id]
        ]));
        // number of follower
        $this->set('follower', $this->Follower->find('count',[
          'conditions' => ['Follower.followed_id' => $id]
        ]));
        // ユーザーと私の関係
        $this->set('follow', $this->Follower->find('count',[
          'conditions' => ['Follower.user_id' => $this->Auth->user('id'),
          'Follower.followed_id' => $id]
        ]));
        $this->set('posts', $this->Post->find('all', $option));
        // プロフィールのユーザー
        $this->set('users', $this->User->find('first', [
          'conditions' => ['User.id' => $id]
        ]
        ));
        // 私
        $this->set('me', $this->User->find('first', [
          'conditions' => ['User.id' => $this->Auth->user('id')]
        ]
        ));
      }

      public function followering($id = null)
      {
        //フォローリスト
        $this->User->id = $id;
        if (!$this->User->exists()) {
         $this->Session->setFlash('unexist user','default', array('class' => 'alert-danger'));
         $this->redirect(['controller'=>'posts','action'=>'index']);
        }
        $this->loadModel('Follower');
        // unbindModel
        $this->User->unbindModel(
        [
          'hasMany' => ['Post']
        ]);
        $option['fields'][] = '*';
        $option['joins'][] = [
        'type' => 'LEFT',
        'table' => 'followers',
        'alias' => 'Follower',
        'conditions' => [
            'User.id = Follower.followed_id',
              'NOT' =>[
               'Follower.followed_id' => $id
              ]
            ]
         ];
        $option['conditions'] = ['Follower.user_id' => $id];
        $option['contain'] = [
         'Follower' => [
          'conditions' =>[
          'Follower.user_id =' => $this->Auth->user('id')
          ]
         ]
        ];
        // フォロ-しているユーザー
        $this->set('followings',$this->User->find('all', $option));
        //プロフィールのユーザー
        $this->set('users', $this->User->find('first', [
          'conditions' => ['User.id' => $id]
        ]
        ));
        // 私
        $this->set('me', $this->User->find('first', [
          'conditions' => ['User.id' => $this->Auth->user('id')]
        ]
        ));
      }

      public function follower($id = null)
      {
        // unexist user
        $this->User->id = $id;
        if (!$this->User->exists()) {
         $this->Session->setFlash('unexist user','default', array('class' => 'alert-danger'));
         $this->redirect(array('controller'=>'posts','action'=>'index'));
        }
        $this->loadModel('Follower');

        $option['joins'][] = [
        'type' => 'LEFT',
        'table' => 'followers',
        'alias' => 'Follower',
        'conditions' => [
          'User.id = Follower.user_id',
          'NOT' => [
            'Follower.user_id' => $id ]
          ]
        ];
        $option['conditions'] = ['Follower.followed_id' => $id];
        $option['contain'] = [
         'Follower' => [
          'conditions' =>[
          'Follower.user_id =' => $this->Auth->user('id')
          ]
         ]
        ];

        $this->set('followers',$this->User->find('all', $option));
        // プロフィールのユーザーと私の関係
        $this->set('follow', $this->Follower->find('count',[
          'conditions' => ['Follower.user_id' => $this->Auth->user('id'),
          'Follower.followed_id' => $id]
        ]));
        //プロフィールのユーザー
        $this->set('users', $this->User->find('first', [
          'conditions' => ['User.id' => $id]
        ]
        ));
        // 私
        $this->set('me', $this->User->find('first', [
          'conditions' => ['User.id' => $this->Auth->user('id')]
        ]
        ));
      }

      public function search()
      {
          $this->set('me', $this->User->find('first', [
           'conditions' => ['User.id' => $this->Auth->user('id')]
               ]
             )
           );
          if (isset($this->request->query['search'])){
            $keyword = $this->request->query['search'];
            $this->Session->write('search', $keyword);
          }

         $this->request->query['search'] = $this->Session->read('search');
         $keyword = $this->request->query['search'];
         $keyword = "%".$keyword."%";
         $paginate['conditions'] = ['User.username LIKE' => $keyword];
         $paginate['contain'] = [
          'Follower' => [
           'conditions' =>[
           'Follower.user_id =' => $this->Auth->user('id')
           ]
          ]
         ];
         try {
           $this->User->unbindModel(
           [
             'hasMany' => ['Post']
           ]);
         $this->Paginator->settings = $paginate;
         $result = $this->Paginator->paginate('User');
         $this->set('result', $result);
         $this->set('count', $this->User->find('count', $paginate));
         $this->set('keyword',$this->request->query['search']);

         } catch (Exception $e) {
          $this->request->params['named']['page'] = 1;
          $result = $this->Paginator->paginate('User');
          $this->set('result', $result);
          $this->set('count', $this->User->find('count', $paginate));
         }
       }

      public function logout()
      {
      $this->Session->setFlash('Log out','default', array('class' => 'alert-danger'));
      return $this->redirect($this->Auth->logout());
      }

}
