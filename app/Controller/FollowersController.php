<?php
App::uses('AppController', 'Controller');

class FollowersController extends AppController
{
    public function follow()
    {
      $this->autoRender = false;
      $this->autoLayout = false;
      if ($this->request->is('ajax')) {

        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $follower = [
          'Follower' => [
          'user_id'=> $user_id,
          'followed_id' => $this->data['name']
          ]
        ];
        $count = $this->Follower->find('count', [
          'conditions'=> [
          'followed_id' => $this->data['name']
          ]
        ]
      );

        $cnt = $this->Follower->find('count', [
          'conditions'=> ['user_id'=> $user_id,
          'followed_id' => $this->data['name'] ]
          ]
        );
        if($cnt == 0){
          $this->Follower->save($follower);
            return $count;
        } else {
          $this->Follower->deleteAll(array('user_id'=> $user_id,'followed_id' => $this->data['name'] ), false);
            return $count-2;
        }
      }
    }
}
