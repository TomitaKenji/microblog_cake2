<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>

<div class="wrapper">
  <div class="side-left">
    <div class="side-title">
      </div>
   <div class="profile-info">
     <?= $this->Html->image($me['User']['image_name'],['class' => 'me-img']); ?>
     <div class="profile-name">
      <?= $me['User']['first_name'] ?>
     </div>

    <a href="#" class="profile-username">
      <?= $this->Html->link("<i class='fas fa-user-alt'></i> Profile",'/users/profile/'.$users['User']['id'],['escape' => false]); ?>
    </a>

    <div class="profile-item">
      <a href="/posts" class="profile-username">
       <i class="fas fa-home"></i> Home
      </a>
    </div>

    <div class="profile-item">
       <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Log Out',
        ['controller' => 'users', 'action' => 'logout'],
        ['escape' => false]
      ); ?>
    </div>
  </div>
</div>

<div class="main">
  <div class="main-title">
    <div class="main-title-contents">
      <a href="#" onClick="history.back(); return false;" style="color:white;"><i class="fas fa-arrow-left"></i></a>
      <a style="color:white;" href="/users/profile/<?= $users['User']['id']?>"><?="@".$users['User']['username'] ?></a>
      <div class="main-title-content">
      </div>
    </div>
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">

      <a class="nav-item nav-link" href="<?="/users/followering/".$users['User']['id']?>" >Following</a>
      <a class="nav-item nav-link" href="<?= "/users/follower/".$users['User']['id'] ?>" style="color: #8C55AA">Follower</a>
    </div>
  </div>
</nav>
<?php if ($followers == null) :?>
  <div class="not-found">
    Not Found
  </div>
<?php endif ; ?>
    <ul>

      <?php foreach ($followers as $follower) : ?>
        <li>
          <div class="post-items">
            <?= $this->Html->image($follower['User']['image_name'],['class' => 'profile-img']); ?>
            <div class="post-item">
              <?= $follower['User']['first_name'];?>
              <?= $this->Html->link( "@".$follower['User']['username'],'/users/profile/'.$follower['User']['id']); ?>
            </div>
          </div>
          <div class="post-content">
            <div class="post-text">
              <?= "Joined ". date('Y-m-d', strtotime(str_replace('-','/', $follower['User']['created']))); ?>
            </div>
            <div class="follow-items">
              <?php if (isset($follower['Follower']['0'])): ?>
               <a href="#" class="follow" data-user-id="<?= $follower['User']['id']?>">Following</a>
              <?php else : ?>
               <a href="#" data-user-id="<?= $follower['User']['id']?>">Follow</a>
              <?php endif ?>
            </div>
          </div>
        </li>
      <hr />
      <?php endforeach; ?>
     </ul>
      </div>

      <div class="side-right">
        <div class="searchform"　>
          <form  name="myForm" action="/posts/search" method="get" onsubmit="return validateForm()">
            <input type="text" name="search" placeholder="Search"/>
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </form>
        </div>
      </div>

<script>
$(function() {
    $('.follow-items > a').click(function(e) {

         var $this = $(this);
         if($this.hasClass("follow")){
           if (confirm('Their Tweet will no loger show up in your home timeline. Is it Okay?')) {
         $.ajax({
              url: "/Followers/follow",
              type: "POST",
              data: { name : $(this).data('user-id') },
              success : function(response){
                  //通信成功時の処理
                    // $('#num').text(response);
                    $this.text('Follow');
                    $this.removeClass('follow');
              },
              error: function(){
                  //通信失敗時の処理
                  alert('Something went wrong');
              }
          });
        }
        } else {

          $.ajax({
               url: "/Followers/follow",
               type: "POST",
               data: { name : $(this).data('user-id') },
               success : function(response){
                   //通信成功時の処理
                     $this.text('Following');
                     $this.addClass('follow');
               },
               error: function(){
                   //通信失敗時の処理
                   alert('Something went wrong');
               }
           });
        //}
      }
        return false;
    });
});

function validateForm() {
  var value = document.forms["myForm"]["search"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty! Try searching username or keywords");
    return false;
  }
}


</script>
