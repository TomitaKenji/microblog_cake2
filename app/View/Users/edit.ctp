<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <?= $this->Html->css('home_styles'); ?>

  <div class="container" style="margin-left:auto; margin-right:auto;">

    <!-- <div class="col"> -->
    <div style="margin-right: -386px; margin-left:300px">
      <div class="main">
        <div class="main-title">
          <a href="/posts" class="profile-username" style="color: white">
           <i class="fas fa-home"></i> HOME
          </a>
        </div>
        <div class="col mb-3">
          <div class="card">
            <div class="card-body">
              <div class="e-profile">
                <div class="row">
                  <div class="col-12 col-sm-auto mb-3">
                    <div class="mx-auto" style="width: 140px;">
                        <?= $this->Html->image($me['User']['image_name'],['class' => 'me-img']); ?>
                    </div>
                  </div>
                  <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                    <div class="text-center text-sm-left mb-2 mb-sm-0">
                      <h4 class=""><?=$me['User']['first_name'] ?></h4>
                      <p class="mb-0"><?="@".$me['User']['username'] ?></p>

                      <div class="mt-2">

                          <form class="form" action="/users/edit" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                          <input type="file" name="data[User][image_name]" />
                      </div>
                    </div>
                    <div class="text-center text-sm-right">

                    </div>
                  </div>
                </div>
                <div class="tab-content pt-3">
                  <div class="tab-pane active">
                   <form class="form" action="/users/edit" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                      <div class="row">
                        <div class="col">
                          <div class="row">
                            <div class="col">
                              <div class="form-group">
                                <span>First Name</span>
                                <input class="form-control" type="text" id="first_name"name="data[User][first_name]" placeholder="<?=$me['User']['first_name'] ?>" value="<?=$me['User']['first_name'] ?>">
                                <div class="feedback1"></div>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-group">
                                <span>Last Name</span>
                                <input class="form-control" type="text" id="last_name" name="data[User][last_name]" placeholder="<?=$me['User']['last_name'] ?>" value="<?=$me['User']['last_name'] ?>">
                                <div class="feedback2"></div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col">
                              <div class="form-group">
                                <span>Username</span>
                                <input class="form-control" type="text" id="username" name="data[User][username]" placeholder="<?=$me['User']['username'] ?>" value="<?=$me['User']['username'] ?>">
                                <div class="feedback3"></div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="row">
                        <div class="col d-flex justify-content-end">
                          <button class="btn btn-primary" type="submit" onsubmit="return validateForm()">Save Changes</button>

                        </div>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- </div> -->
  <!-- </div> -->
  </div>
<script>

</script>
