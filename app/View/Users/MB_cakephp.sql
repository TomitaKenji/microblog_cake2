-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- ホスト: localhost
-- 生成日時: 2019 年 11 月 24 日 05:44
-- サーバのバージョン： 5.6.45
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `MB_cakephp`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `comment`, `created`, `modified`, `status`) VALUES
(1, 0, 4, 'asddf', '2019-11-22 05:14:45', '2019-11-22 05:14:45', 1),
(2, 0, 4, 'comment!!1', '2019-11-22 05:15:02', '2019-11-22 05:15:02', 1),
(3, 0, 4, 'comment!', '2019-11-22 05:15:09', '2019-11-22 05:15:09', 1),
(4, 0, 9, 'Add comment!', '2019-11-22 05:18:47', '2019-11-22 05:18:47', 1),
(5, 0, 9, 'add comment!!', '2019-11-22 05:18:56', '2019-11-22 05:18:56', 1),
(6, 0, 10, 'test comment', '2019-11-22 05:28:21', '2019-11-22 05:28:21', 1),
(7, 0, 7, 'comment!', '2019-11-22 05:31:59', '2019-11-22 05:31:59', 1),
(8, 0, 10, 'comment', '2019-11-22 05:32:14', '2019-11-22 05:32:14', 1),
(9, 0, 10, 'Add comment', '2019-11-22 05:43:53', '2019-11-22 05:43:53', 1),
(10, 0, 10, 'Add comment', '2019-11-22 05:44:02', '2019-11-22 05:44:02', 1),
(11, 0, 10, 'comment', '2019-11-22 05:44:31', '2019-11-22 05:44:31', 1),
(12, 0, 12, 'Add comment', '2019-11-22 05:46:24', '2019-11-22 05:46:24', 1),
(13, 0, 13, 'add comment', '2019-11-22 05:47:28', '2019-11-22 05:47:28', 1),
(14, 0, 14, 'comment', '2019-11-22 05:48:42', '2019-11-22 05:48:42', 1),
(15, 0, 15, 'Add comment!', '2019-11-22 05:49:52', '2019-11-22 05:49:52', 1),
(16, 0, 16, 'aaa', '2019-11-22 05:55:46', '2019-11-22 05:55:46', 1),
(17, 0, 17, 'comment!!', '2019-11-22 05:56:35', '2019-11-22 05:56:35', 1),
(18, 0, 17, 'comment!', '2019-11-22 05:56:40', '2019-11-22 05:56:40', 1),
(19, 0, 17, 'comment!', '2019-11-22 05:56:49', '2019-11-22 05:56:49', 1),
(20, 0, 18, 'aaaa', '2019-11-22 06:03:25', '2019-11-22 06:03:25', 1),
(21, 0, 18, 'Add comment', '2019-11-22 06:04:54', '2019-11-22 06:04:54', 1),
(22, 0, 19, 'Add comment!!', '2019-11-22 06:09:32', '2019-11-22 06:09:32', 1),
(23, 0, 19, '111111', '2019-11-22 06:10:23', '2019-11-22 06:10:23', 1),
(24, 0, 19, 'comment', '2019-11-22 06:14:52', '2019-11-22 06:14:52', 1),
(25, 0, 19, 'add\r\n', '2019-11-22 06:15:37', '2019-11-22 06:15:37', 1),
(26, 0, 19, 'commetn', '2019-11-22 06:19:20', '2019-11-22 06:19:20', 1),
(27, 0, 19, 'aaa', '2019-11-22 06:19:51', '2019-11-22 06:19:51', 1),
(28, 0, 19, 'aaa', '2019-11-22 06:20:49', '2019-11-22 06:20:49', 1),
(29, 0, 19, 'comment', '2019-11-22 06:25:20', '2019-11-22 06:25:20', 1),
(30, 0, 19, 'aaa', '2019-11-22 06:26:48', '2019-11-22 06:26:48', 1),
(31, 0, 19, 'comment', '2019-11-22 06:38:15', '2019-11-22 06:38:15', 1),
(32, 0, 19, 'comment', '2019-11-22 06:39:42', '2019-11-22 06:39:42', 1),
(33, 0, 20, 'comment', '2019-11-22 06:50:12', '2019-11-22 06:50:12', 1),
(34, 0, 20, 'comment', '2019-11-22 06:51:24', '2019-11-22 06:51:24', 1),
(35, 0, 20, 'comment', '2019-11-22 06:53:08', '2019-11-22 06:53:08', 1),
(36, 0, 20, '1111', '2019-11-22 07:10:22', '2019-11-22 07:10:22', 1),
(37, 0, 35, 'Comment', '2019-11-23 03:35:19', '2019-11-23 03:35:19', 1),
(38, 0, 35, 'ã‚ã‚ã‚ã‚ã‚', '2019-11-23 03:42:13', '2019-11-23 03:42:13', 1),
(39, 0, 47, 'aaaaa', '2019-11-23 07:26:03', '2019-11-23 07:26:03', 1),
(40, 0, 47, 'iine', '2019-11-23 07:26:21', '2019-11-23 07:26:21', 1),
(43, 0, 48, 'aaaaa', '2019-11-23 08:31:04', '2019-11-23 08:31:04', 1),
(46, 4, 53, 'aaaaa', '2019-11-23 11:37:40', '2019-11-23 11:37:40', 1),
(47, 4, 54, 'aaaaa', '2019-11-23 12:18:21', '2019-11-23 12:18:21', 1),
(48, 4, 54, 'aaaaa', '2019-11-23 12:18:28', '2019-11-23 12:18:28', 1),
(49, 4, 53, '2222222\r\n', '2019-11-23 12:23:45', '2019-11-23 12:23:45', 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `followers`
--

CREATE TABLE `followers` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `followed_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `followed_id`) VALUES
(3, 1, 2),
(4, 2, 3),
(9, 4, 4);

-- --------------------------------------------------------

--
-- テーブルの構造 `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`) VALUES
(2, 1, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `posts`
--

CREATE TABLE `posts` (
  `id` bigint(50) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post` varchar(255) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `edited` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `post`, `image_name`, `created`, `edited`, `deleted`) VALUES
(46, 0, 'aaaa', 'images.jpeg', '2019-11-23 06:08:15', NULL, NULL),
(48, 0, 'aaaa', 'irish.jpg', '2019-11-23 07:01:04', NULL, NULL),
(50, 0, 'edit post', '', '2019-11-23 07:07:15', NULL, NULL),
(51, 0, 'post with image', 'cheese.jpg', '2019-11-23 10:02:32', NULL, NULL),
(52, 0, 'post', '', '2019-11-23 10:30:28', NULL, NULL),
(54, 4, 'test', 'ham.jpg', '2019-11-23 11:26:09', NULL, NULL),
(55, 4, 'sdadfadfasd', 'images.png', '2019-11-23 14:15:13', NULL, NULL),
(56, 4, 'make sure', 'ham.jpg', '2019-11-23 15:23:02', NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(128) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `image_name` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `active`, `image_name`, `created`, `modified`) VALUES
(2, 'Kanji', 'tora169mm@icloud.com', '466836931ce92e6f025d20a5dce3ae21516f19f7', 0, NULL, '2019-11-22 13:11:49', '2019-11-22 13:11:49'),
(4, 'Kenji', 'tora169mm@gmail.com', '466836931ce92e6f025d20a5dce3ae21516f19f7', 1, 'é»’-æ˜Ÿ-ã‚¤ãƒ©ã‚¹ãƒˆ__k20647236.jpg', '2019-11-22 13:50:25', '2019-11-23 10:06:47');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- テーブルのAUTO_INCREMENT `followers`
--
ALTER TABLE `followers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- テーブルのAUTO_INCREMENT `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- テーブルのAUTO_INCREMENT `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- テーブルのAUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
