<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>


<div class="wrapper">
  <div class="side-left">
    <div class="side-title">
    </div>

   <div class="profile-info">
     <?= $this->Html->image($me['User']['image_name'],['class' => 'me-img']); ?>
     <div class="profile-name">
      <?= $me['User']['first_name'] ?>
     </div>

     <div class="profile-username">
       <?= $this->Html->link("<i class='fas fa-user-alt'></i> Profile",'/users/profile/'.$me['User']['id'],['escape' => false]); ?>
     </div>

    <div class="profile-item">
      <a href="/posts" class="profile-username">
       <i class="fas fa-home"></i>Home
      </a>
    </div>

    <div class="profile-item">

    </div>
    <div class="profile-item">
       <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Log Out',
        ['controller' => 'users', 'action' => 'logout'],
        ['escape' => false]
      ); ?>
    </div>
  </div>
</div>

<div class="main">
  <div class="main-title">
    <a href="#" onClick="history.back(); return false;" style="color:white;"><i class="fas fa-arrow-left"></i></a>
    <div class="searchform">

    </div>
  </div>

    <ul>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link" href="/posts/search?search=<?= $keyword ?>">Post</a>
            <a class="nav-item nav-link" href="#" style="color:#8C55AA;">User</a>
          </div>
        </div>
      </nav>

      <?php if ($result == null) :?>
        <div class="not-found">
          No results</br>
          Nothing came up for that search.
        </div>
      <?php endif ; ?>
      <ul>
      <?php foreach ($result as $result) : ?>
        <li>
          <div class="post-items">
            <?= $this->Html->image($result['User']['image_name'],['class' => 'profile-img']); ?>
            <div class="post-item">
              <?= $result['User']['first_name'];?>
              <?= $this->Html->link("@".$result['User']['username'],'/users/profile/'.$result['User']['id']); ?>
            </div>
          </div>
          <div class="post-content">
            <div class="post-text">
              <?= "Joined ".date('Y-m-d', strtotime(str_replace('-','/', $result['User']['created']))); ?>
            </div>
            <div class="follow-items">
            <?php if ($me['User']['id'] != $result['User']['id']) : ?>
              <?php if (isset($result['Follower']['0'])): ?>
               <a href="#" class="follow" data-user-id="<?= $result['User']['id']?>">Following</a>
              <?php else : ?>
               <a href="#" data-user-id="<?= $result['User']['id']?>">Follow</a>
              <?php endif ?>
            <?php endif ?>
            </div>
          </div>
        </li>
        <hr />
      <?php endforeach; ?>
     </ul>
     <div class="paging-btn">
     <?php if ($result != null) :?>
       <?php if ($this->Paginator->hasPrev()): ?>
        <?= $this->Paginator->prev('Prev', array(), null, array('class' => 'prev disabled')); ?>
       <?php endif ;?>
     <?= $this->Paginator->numbers(); ?>
       <?php if ($this->Paginator->hasNext()): ?>
        <?= $this->Paginator->next('Next', array(), null, array('class' => 'prev disabled')); ?>
       <?php endif ;?>
     <?php endif ; ?>
   </div>
  </div>

<div class="side-right" style="height: 130px;">
  <div class="searchform">
    <form name="myForm" action="/users/search" method="get" onsubmit="return validateForm()">
      <input type="text" name="search" placeholder="Search"/>
      <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
      </button>
    </form>
  </div>
  <i class="fa fa-search" aria-hidden="true"></i><?=" : ".$this->Session->read('search')?></br>
  <?= "About : ".$count ." results" ;?>
</div>
<script>
function validateForm() {
  var value = document.forms["myForm"]["search"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty! Try searching username or keywords");
    return false;
  }
}

$(function() {
    $('.follow-items > a').click(function(e) {
         var $this = $(this);
         if($this.hasClass("follow")){
           if (confirm('Their Tweet will no loger show up in your home timeline. Is it Okay?')) {
         $.ajax({
              url: "/Followers/follow",
              type: "POST",
              data: { name : $(this).data('user-id') },
              success : function(response){
                  //通信成功時の処理
                    $this.text('Follow');
                    $this.removeClass('follow');
              },
              error: function(){
                  //通信失敗時の処理
                  alert('Something went wrong');
              }
          });
         }
        } else {
          $.ajax({
               url: "/Followers/follow",
               type: "POST",
               data: { name : $(this).data('user-id') },
               success : function(response){
                   //通信成功時の処理
                     // $('#num').text(response);
                     $this.text('Following');
                     $this.addClass('follow');
               },
               error: function(){
                   //通信失敗時の処理
                   alert('Something went wrong');
               }
           });
         }
        return false;
    });
});

// $('.follow')
//                 .mouseover(function() {
//                     $(this).css('background', 'red');
//                     $(this).css('color', 'white');
//                     $(this).text('Unfollow');
//                 })
//                 .mouseout(function() {
//                     $(this).css('background', '#8C55AA');
//                     $(this).text('Following');
//                 })
//                 .click(function(e) {
//                   $(this).css('background', '#8C55AA');
//                   $(this).text('follow');
//                     // $(this).text(e.pageX);
//                 });
</script>
