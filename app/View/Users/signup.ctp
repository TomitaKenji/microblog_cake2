<?= $this->Html->css('css_style'); ?>

<div class="main">
  <p class="sign" align="center">Sign up</p>
<?php
echo $this->Form->create('User', array('novalidate' => true));
echo $this->Form->input('username', array('label' => 'Username', 'class' =>'sign-input'));
echo $this->Form->input('first_name', array('label' => 'First name', 'class' =>'sign-input'));
echo $this->Form->input('last_name', array('label' => 'Last name', 'class' =>'sign-input'));
echo $this->Form->input('email', array('label' => 'E-mail', 'class' =>'sign-input'));
echo $this->Form->input('password', array('label' => 'Password', 'class' =>'sign-input'));
echo $this->Form->input('password_confirm', array('label' => 'Confirmed Password', 'type' => 'password', 'class' =>'sign-input'));
echo $this->Form->end('Sign up');
?>
<p class="forgot" align="center"><a href="/users/login">Log in</a></p>
</div>
