<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>


<div class="wrapper">
  <div class="side-left">
    <div class="side-title">
      </div>
   <div class="profile-info">
     <?= $this->Html->image($me['User']['image_name'],['class' => 'me-img']); ?>
     <div class="profile-name">
      <?= $me['User']['first_name'] ?>
     </div>

     <div class="profile-username">
       <!-- <?= $this->Html->link("@".$me['User']['username'],'/users/profile/'.$me['User']['id']); ?> -->
     </div>

    <div class="profile-item">
      <a href="/posts" class="profile-username">
       <i class="fas fa-home"></i>Home
      </a>
    </div>

    <div class="profile-item">
       <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Log Out',
        ['controller' => 'users', 'action' => 'logout'],
        ['escape' => false]
      ); ?>
    </div>
  </div>
</div>


<div class="main">
  <div class="main-title">
   <a href="#" onClick="history.back(); return false;" style="color:white;"><i class="fas fa-arrow-left"></i></a>
  </div>
    <div class="profile-info" style="margin: 0 0 0 0;">
      <div class="post-items" style="padding: 30px 15px 0 15px; margin: 0 0 0 0; height: 200px;">
        <?= $this->Html->image($users['User']['image_name'],['class' => 'me-img']); ?>
        <div class="post-item">
          <?= $users['User']['first_name'];?>
          <?= $this->Html->link("@".$users['User']['username'],'/users/profile/'.$users['User']['id']); ?>

          <?= "Joined ". date('Y-m-d', strtotime(str_replace('-','/', $users['User']['created']))); ?>
          <div class="follow-content">
            <a href="<?= "/users/followering/".$users['User']['id'] ?>"><?= $following-1; ?> Following</a>
            <a href="<?= "/users/follower/".$users['User']['id'] ?>"><span id="num"><?= $follower-1; ?></span> Follower</a>
          </div>
        </div>
        <div class="follow-item">
          <?php if ($me['User']['id'] != $users['User']['id']) :?>
           <?php if ($follow == 0 ):?>
            <a href="#"  data-user-id="<?= $users['User']['id']?>">Follow</a>
           <?php else: ?>
             <a href="#" class="follow" data-user-id="<?= $users['User']['id']?>">Following</a>
           <?php endif; ?>
         <?php else: ?>
           <div class="editprofile">
             <?= $this->Html->link("<i class='fas fa-user-alt'></i> Edit Profile",
             ['controller' => 'users', 'action' => 'edit'],
             ['escape' => false]
           ); ?>
           </div>
         <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="profile-border">
    </div>

       <ul>
         <?php foreach ($posts as $post) : ?>
           <div class="modal fade" id=<?="demoNormalModal".$post['Post']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
             <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title" id="demoModalTitle">Retweet</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                       <form action="/posts/share" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                       <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                         <input type='hidden' name="data[Post][id]" value="<?= $post['Post']['id']?>">
                         <textarea name="data[Post][post]" rows="3" cols="30" id="PostPost"></textarea>
                         <div class="input file">
                           <input type="file" name="data[Post][image_name]" id="PostImageName">
                         </div>
                         <div class="modal-post">
                           <div class="post-items">
                             <?= $this->Html->image($post['User']['image_name'],['class' => 'share-img']);?>
                             <div class="post-item">
                               <?= $this->Html->link($post['User']['first_name'],'/users/profile/'.$post['User']['id']); ?>
                               <?= $this->Html->link("@".$post['User']['username'],'/users/profile/'.$post['User']['id']); ?>
                             </div>
                           </div>
                            <div class="post-content">
                             <div class="post-text">
                              <?= $this->Html->link($post['Post']['post'],'/posts/view/'.$post['Post']['id']); ?>
                             </div>
                               <?php if ($post['Post']['image_name'] != null) :?>
                                <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img'] );?>
                               <?php endif; ?>
                            </div>
                          </div>
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Retweet</button>
                       </form>
                     </div>
                 </div>
             </div>
         </div>

           <li>
             <div class="post-items">
               <?= $this->Html->image($post['User']['image_name'],['class' => 'profile-img']); ?>
               <div class="post-item">
                 <?= $post['User']['first_name'];?>
                 <?= $this->Html->link("@".$post['User']['username'],'/users/profile/'.$post['User']['id']); ?>
               </div>
             </div>
             <div class="post-content">
               <div class="post-text">
                  <?= nl2br(h($post['Post']['post'])); ?>
               </div>
             <?php if ($post['Post']['image_name'] != null):?>
              <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img']);?>
             <?php endif; ?>
             </div>

             <?php if ($post['Share']['id'] != null): ?>
             <div class="share">
               <div class="share-items">
                 <?= $this->Html->image($post['ShareUser']['image_name'],['class' => 'share-img']);?>
                 <div class="share-item">
                   <?= $post['ShareUser']['first_name']; ?>
                   <?= $this->Html->link("@".$post['ShareUser']['username'],'/users/profile/'.$post['ShareUser']['id']); ?>
                 </div>
               </div>
                  <div class="share-content">
                   <div class="share-text">
                    <?= nl2br(h($post['Share']['post'])); ?>
                   </div>
                     <?php if ($post['Share']['image_name'] != null) :?>
                      <?= $this->Html->image($post['Share']['image_name'],['class' => 'post-img'] );?>
                     <?php endif; ?>
                  </div>
             </div>
             <?php endif; ?>

              <?php if ($follow != 0 ):?>
             <div class="post-btn">
               <?= $this->Form->postLink('<i class="far fa-comment"></i>','/posts/view/'.$post['Post']['id'],
               ['escape' => false]
             ); ?>

               <a href="#" data-toggle="modal" data-target=<?="#demoNormalModal".$post['Post']['id']?>><i class="fas fa-retweet"></i></a>

               <?php if ($post['Like']) :?>
                 <div class="like-btn">
                  <a href="" class="unlike" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
                  <span><?= $post['Post']['like_count'] ?></span>
                 </div>

               <?php else: ?>
                 <div class="like-btn">
                  <a href="" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
                  <span><?= $post['Post']['like_count'] ?></span>
                 </div>
               <?php endif; ?>
             </div>
           <?php endif; ?>
             <hr />
           </li>
         <?php endforeach; ?>
        </ul>
        <div class="main-title">

          </div>
      </div>

      <div class="side-right">
        <div class="searchform"　>
          <form  name="myForm" action="/posts/search" method="get" onsubmit="return validateForm()">
            <input type="text" name="search" placeholder="Search"/>
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </form>
        </div>
      </div>

<script>
   $(function() {
       $('.follow-item > a').click(function(e) {

            var $this = $(this);
            if($this.hasClass("follow")){
              if (confirm('Their Tweet will no loger show up in your home timeline. Is it Okay?')) {
            $.ajax({
                 url: "/Followers/follow",
                 type: "POST",
                 data: { name : $(this).data('user-id') },
                 success : function(response){
                     //通信成功時の処理
                       $('#num').text(response);
                       $this.text('Follow');
                       $this.removeClass('follow');
                       $('.post-btn').fadeOut('fast');
                 },
                 error: function(){
                     //通信失敗時の処理
                     alert('Something went wrong');
                 }
             });
           }
           } else {
             $.ajax({
                  url: "/Followers/follow",
                  type: "POST",
                  data: { name : $(this).data('user-id') },
                  success : function(response){
                      //通信成功時の処理
                        $('#num').text(response);
                        $this.text('Following');
                        $this.addClass('follow');
                        $('.post-btn').fadeIn();

                  },
                  error: function(){
                      //通信失敗時の処理
                      alert('Something went wrong');
                  }
              });
         }
           return false;
       });
   });

   $(function() {
       $('.like-btn > a').click(function(e) {
           // if (confirm('sure?')) {
            var $this = $(this);
              if($this.hasClass("unlike")){
            $.ajax({
                 url: "/Likes/like",
                 type: "POST",
                 data: { name : $(this).data('post-id') },
                 success : function(response){
                     //通信成功時の処理
                       $this.removeClass('unlike');
                       $this.next().html(response);
                 },
                 error: function(){
                     //通信失敗時の処理
                     alert('Something went wrong');
                 }
             });
           } else{
             $.ajax({
                  url: "/Likes/like",
                  type: "POST",
                  data: { name : $(this).data('post-id') },
                  success : function(response){
                      //通信成功時の処理
                        $this.addClass('unlike');
                        $this.next().html(response);
                  },
                  error: function(){
                      //通信失敗時の処理
                      alert('Something went wrong');
                  }
              });
             }
           return false;
       });
   });

   function validateForm() {
     var value = document.forms["myForm"]["search"].value;
     var reg = new RegExp(/^\s+$/);
     if (value == "") {
       alert("Try searching username or keywords");
       return false;
     }
     var regex = new RegExp('[¥¥s]');
     if (reg.test(value)){
       alert("cant make empty! Try searching username or keywords");
       return false;
     }
   }

   $(function() {

  //画像にマウスを乗せたら発動
  $('.follow').hover(function() {

    //画像のsrc属性が別画像のパスに切り替わる
    $(this).css('background-color', 'red');
    $(this).text('Unfollow');
    $(this).css('color', 'white');

  //ここにはマウスを離したときの動作を記述
  }, function() {

    //画像のsrc属性を元の画像のパスに戻す
    $(this).css('background-color', '#8C55AA');
    $(this).text('Following');

  });
});
</script>
