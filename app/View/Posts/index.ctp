<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>
<?= $this->Flash->render() ?>

<!-- Model dialog-->
<div class="modal fade" id="demoNormalModal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="demoModalTitle">POST</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form action="/posts/add" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>

              <textarea name="data[Post][post]" rows="3"  cols="30" id="PostPost"></textarea>
              <div class="input file">
                <input type="file" name="data[Post][image_name]" id="PostImageName">
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">POST</button>
            </form>
          </div>
        </div>
      </div>
  </div>

<div class="wrapper">
  <div class="side-left">
    <div class="side-title">

      </div>
   <div class="profile-info">
     <?= $this->Html->image($users['User']['image_name'],['class' => 'me-img']); ?>
     <div class="profile-name">
      <?= $users['User']['first_name'] ?>
     </div>

    <div class="profile-username">
      <?= $this->Html->link("<i class='fas fa-user-alt'></i> Profile",'/users/profile/'.$users['User']['id'],['escape' => false]); ?>
    </div>

    <div class="profile-item">
       <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Log Out',
        ['controller' => 'users', 'action' => 'logout'],
        ['escape' => false]
      ); ?>
    </div>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#demoNormalModal">
      POST
    </button>
  </div>
</div>

<div class="main">
  <div class="main-title">
    <h4>HOME</h4>
    </div>

    <ul>
      <?php foreach ($posts as $post) : ?>

        <div class="modal fade" id=<?="demoNormalModal".$post['Post']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="demoModalTitle">Retweet</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <form action="/posts/share" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                      <input type='hidden' name="data[Post][id]" value="<?= $post['Post']['id']?>">
                      <textarea name="data[Post][post]" rows="3" maxlength="140" cols="30" id="PostPost" placeholder="What's happening"></textarea>
                      <div class="input file">
                        <input type="file" name="data[Post][image_name]" id="PostImageName">
                      </div>
                      <div class="modal-post">
                        <div class="post-items">
                          <?= $this->Html->image($post['User']['image_name'],['class' => 'share-img']);?>
                          <div class="post-item" style="font-weight: bold;">
                            <?= $post['User']['first_name']; ?></br>
                            <?= "@".$post['User']['username']; ?>
                          </div>
                        </div>
                         <div class="post-content">
                          <div class="post-text">
                           <?=  nl2br(h($post['Post']['post'])); ?>
                          </div>
                            <?php if ($post['Post']['image_name'] != null) :?>
                             <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img'] );?>
                            <?php endif; ?>
                         </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Retweet</button>
                    </form>
                  </div>
              </div>
          </div>
      </div>
     <!--Edit Model dialog-->
    <div class="modal fade" id=<?="editNormalModal".$post['Post']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="demoModalTitle">EDIT</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <form action="/posts/edit" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                  <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                    <input type='hidden' name="data[Post][id]" value="<?= $post['Post']['id']?>">
                    <textarea name="data[Post][post]" rows="3" maxlength="140" cols="30" id="PostPost"><?= $post['Post']['post']?></textarea>
                    <div class="input file">
                      <input type="file" name="data[Post][image_name]" id="PostImageName">
                    </div>
                    <div class="modal-post">
                      <div class="post-items">
                        <?= $this->Html->image($post['User']['image_name'],['class' => 'share-img']);?>
                        <div class="post-item" style="font-weight: bold;">
                          <?= $post['User']['first_name']; ?></br>
                          <?= "@".$post['User']['username']; ?>
                        </div>
                      </div>
                       <div class="post-content">
                        <div class="post-text">
                         <?= nl2br(h($post['Post']['post'])); ?>
                        </div>
                          <?php if ($post['Post']['image_name'] != null) :?>
                           <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img'] );?>
                          <?php endif; ?>
                       </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">EDIT</button>
                  </form>
                </div>
            </div>
        </div>
      </div>

        <li>
          <div class="post-items">
            <?= $this->Html->image($post['User']['image_name'],['class' => 'profile-img']); ?>
            <div class="post-item">
              <?= $post['User']['first_name'];?>
              <?= $this->Html->link( '@'.$post['User']['username'],'/users/profile/'.$post['User']['id']); ?>
            </div>

            <div class="date-item">
              <?= date('M j(D) H:i', strtotime($post['Post']['created']))?>
            <?php if ($post['Post']['created'] != $post['Post']['modified']):?>
             <i class="fas fa-pen"></i>
            <?php endif; ?>
            </div>
          </div>
          <div class="post-content">
            <div class="post-text">
              <?= nl2br(h($post['Post']['post'])); ?>
            </div>
              <?php if ($post['Post']['image_name'] != null):?>
               <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img']);?>
              <?php endif; ?>
          </div>

          <?php if ($post['Share']['id'] != null): ?>
            <div class="share">
              <div class="share-items">
                <?= $this->Html->image($post['ShareUser']['image_name'],['class' => 'share-img']);?>
                <div class="share-item">
                  <?= $this->Html->link($post['ShareUser']['first_name'],'/users/profile/'.$post['ShareUser']['id']); ?>
                  <?= $this->Html->link("@".$post['ShareUser']['username'],'/users/profile/'.$post['ShareUser']['id']); ?>
                </div>
            　</div>
               <div class="share-content">
                 <?php if ($post['Share']['deleted'] != null): ?>
                   <div class="share-text">
                    deleted post and image!
                   </div>
                 <?php else: ?>
                <div class="share-text">
                 <?= $this->Html->link($post['Share']['post'],'/posts/view/'.$post['Share']['id']); ?>
                </div>
                  <?php if ($post['Share']['image_name'] != null) :?>
                   <?= $this->Html->image($post['Share']['image_name'],['class' => 'post-img'] );?>
                  <?php endif; ?>
                <?php endif;?>
               </div>
            </div>
          <?php endif; ?>

          <div class="post-btn">
             <?= $this->Form->postLink('<i class="far fa-comment"></i>','/posts/view/'.$post['Post']['id'],
             ['escape' => false]
           ); ?>

          <a href="#" data-toggle="modal" data-target=<?="#demoNormalModal".$post['Post']['id']?>><i class="fas fa-retweet"></i></a>
          <?php if ($post['Like']) :?>
            <div class="like-btn">
             <a href="#" class="unlike" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
          <span><?= $post['Post']['like_count']?></span>
          </div>
          <?php else: ?>
            <div class="like-btn">
             <a href="#" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
             <span><?= $post['Post']['like_count']?></span>
            </div>
          <?php endif; ?>

          <?php if ($users['User']['id'] == $post['Post']['user_id']) :?>
          <a href="#" data-toggle="modal" data-target=<?="#editNormalModal".$post['Post']['id']?>><i class="fas fa-edit"></i></a>
          <?php endif; ?>

          <?php if ($users['User']['id'] == $post['Post']['user_id']) :?>
            <?= $this->Form->postLink('<i class="fas fa-trash-alt"></i>', ['action'=>'delete', $post['Post']['id']],
            ['escape' => false]
          );?>
          <?php endif; ?>
          </div>
          <hr />
        </li>
      <?php endforeach; ?>
     </ul>
     <div class="paging-btn">
       <?php if ($posts != null) :?>
         <?php if ($this->Paginator->hasPrev()): ?>
          <?= $this->Paginator->prev('Prev', array(), null, array('class' => 'prev disabled')); ?>
         <?php endif ;?>
       <?= $this->Paginator->numbers(); ?>
         <?php if ($this->Paginator->hasNext()): ?>
          <?= $this->Paginator->next('Next', array(), null, array('class' => 'prev disabled')); ?>
         <?php endif ;?>
       <?php endif ; ?>
     </div>
   </div>
  <div class="side-right">
    <div class="searchform"　>
      <form  name="myForm" action="/posts/search" method="get" onsubmit="return validateForm()">
        <input type="text" name="search" placeholder="Search"/>
        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
        </button>
      </form>
    </div>
  </div>

<script>
$(function() {
  $('.alert-success').fadeOut(2000);

});
$(function() {
    $('.like-btn > a').click(function(e) {
        // if (confirm('sure?')) {
         var $this = $(this);
           if($this.hasClass("unlike")){
         $.ajax({
              url: "/Likes/like",
              type: "POST",
              data: { name : $(this).data('post-id') },
              success : function(response){
                  //通信成功時の処理
                    $this.removeClass('unlike');
                    $this.next().html(response);
              },
              error: function(){
                  //通信失敗時の処理
                  alert('Something went wrong');
              }
          });
        } else{
          $.ajax({
               url: "/Likes/like",
               type: "POST",
               data: { name : $(this).data('post-id') },
               success : function(response){
                   //通信成功時の処理
                     $this.addClass('unlike');
                     $this.next().html(response);
               },
               error: function(){
                   //通信失敗時の処理
                   alert('Something went wrong');
               }
           });
          }
        return false;
    });
});

function validateForm() {
  var value = document.forms["myForm"]["search"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty! Try searching username or keywords");
    return false;
  }
}

</script>
