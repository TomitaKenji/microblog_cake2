<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>


<div class="wrapper">
    <div class="side-left">
      <div class="side-title">
      </div>

     <div class="profile-info">
       <?= $this->Html->image($me['User']['image_name'],['class' => 'me-img']); ?>
       <div class="profile-name">
        <?= $me['User']['first_name'] ?>
       </div>

       <div class="profile-username">
         <?= $this->Html->link("<i class='fas fa-user-alt'></i> Profile",'/users/profile/'.$me['User']['id'],['escape' => false]); ?>
       </div>

      <div class="profile-item">
        <a href="/posts" class="profile-username">
         <i class="fas fa-home"></i>Home
        </a>
      </div>

      <div class="profile-item">
         <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Log Out',
          ['controller' => 'users', 'action' => 'logout'],
          ['escape' => false]
        ); ?>
      </div>
    </div>
  </div>

<div class="main">
  <div class="main-title">
   <a href="#" onClick="history.back(); return false;" style="color:white;"><i class="fas fa-arrow-left"></i></a>
  </div>

    <ul>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link" href="" style="color:#8C55AA;">Post</a>
            <a class="nav-item nav-link" href="/users/search?search=<?= $keyword ?>">User</a>
          </div>
        </div>
      </nav>
      <?php if ($result == null) :?>
        <div class="not-found">
          No results</br>
          Nothing came up for that search.
        </div>

      <?php endif ; ?>
      <?php foreach ($result as $result) : ?>

          <div class="post-items">
            <?= $this->Html->image($result['User']['image_name'],['class' => 'profile-img']); ?>
            <div class="post-item">
              <?= $result['User']['first_name'];?>
              <?= $this->Html->link("@".$result['User']['username'],'/users/profile/'.$result['User']['id']); ?>
            </div>
          </div>
          <div class="post-content">
            <div class="post-text">
              <?= $this->Html->link($result['Post']['post'],'/posts/view/'.$result['Post']['id']); ?>
            </div>
              <?php if ($result['Post']['image_name'] != null):?>
               <?= $this->Html->image($result['Post']['image_name'],['class' => 'post-img']);?>
              <?php endif; ?>
          </div>

          <?php if ($result['Share']['id'] != null): ?>
            <div class="share">

              <div class="share-items">
                <?= $this->Html->image($result['ShareUser']['image_name'],['class' => 'share-img']);?>
                <div class="share-item">
                  <?= $result['ShareUser']['first_name']?>
                  <?= $this->Html->link("@".$result['ShareUser']['username'],'/users/profile/'.$result['ShareUser']['id']); ?>
                </div>
              </div>

               <div class="share-content">
                 <?php if ($result['Share']['deleted'] != null): ?>
                   <div class="share-text">
                    deleted post and image!
                   </div>
                 <?php else: ?>
                <div class="share-text">
                 <?= $this->Html->link($result['Share']['post'],'/posts/view/'.$result['Share']['id']); ?>
                </div>
                  <?php if ($result['Share']['image_name'] != null) :?>
                   <?= $this->Html->image($result['Share']['image_name'],['class' => 'post-img'] );?>
                  <?php endif; ?>
                <?php endif; ?>
              </div>
            </div>
          <?php endif; ?>
        </li>
      <hr />
      <?php endforeach; ?>
    </ul>
     <div class="paging-btn">
       <?php if ($result != null) :?>
         <?php if ($this->Paginator->hasPrev()): ?>
          <?= $this->Paginator->prev('Prev', array(), null, array('class' => 'prev disabled')); ?>
         <?php endif ;?>
       <?= $this->Paginator->numbers(); ?>
         <?php if ($this->Paginator->hasNext()): ?>
          <?= $this->Paginator->next('Next', array(), null, array('class' => 'prev disabled')); ?>
         <?php endif ;?>
       <?php endif ; ?>
     </div>
   </div>

   <div class="side-right" style="height: 130px;">
     <div class="searchform"　>
       <form  name="myForm" action="/posts/search" method="get" onsubmit="return validateForm()">
         <input type="text" name="search" placeholder="Search"/>
         <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
         </button>
       </form>
     </div>
     <i class="fa fa-search" aria-hidden="true"></i><?=" : ".h($this->Session->read('search'))?></br>
     <?= "About : ".$count ." results" ;?>
   </div>
<script>
function validateForm()
{
  var value = document.forms["myForm"]["search"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty! Try searching username or keywords");
    return false;
  }
}
</script>
