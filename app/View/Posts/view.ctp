
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<?= $this->Html->css('home_styles'); ?>

<!-- Modal -->
<div class="modal fade" id="demoNormalModal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="demoModalTitle">POST</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form action="/posts/add" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>

              <textarea name="data[Post][post]" rows="3" maxlength="140" cols="30" id="PostPost"></textarea>
              <div class="input file">
                <input type="file" name="data[Post][image_name]" id="PostImageName">
              </div>
            </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">POST</button>
            </form>
          </div>
      </div>
  </div>
</div>

<div class="wrapper">
  <div class="side-left">
    <div class="side-title">

      </div>
   <div class="profile-info">
     <?= $this->Html->image($users['User']['image_name'],['class' => 'me-img']); ?>
     <div class="profile-name">
      <?= $users['User']['first_name'] ?>
     </div>

     <div class="profile-username">
       <?= $this->Html->link("<i class='fas fa-user-alt'></i> Profile",'/users/profile/'.$users['User']['id'],['escape' => false]); ?>
     </div>

    <div class="profile-item">
      <a href="/posts" class="profile-username">
       <i class="fas fa-home"></i> Home
      </a>
    </div>


  <div class="profile-item">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#demoNormalModal">
      POST
    </button>
  </div>
</div>
</div>

<div class="modal fade" id=<?="demoNormalModal".$post['Post']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="demoModalTitle">Retweet</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form action="/posts/share" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
              <input type='hidden' name="data[Post][id]" value="<?= $post['Post']['id']?>">
              <textarea name="data[Post][post]" rows="3" maxlength="140" cols="30" id="PostPost"></textarea>
              <div class="input file">
                <input type="file" name="data[Post][image_name]" id="PostImageName">
              </div>
              <div class="modal-post">
                <div class="post-items">
                  <?= $this->Html->image($post['User']['image_name'],['class' => 'share-img']);?>
                  <div class="post-item" style="font-weight: bold;">
                    <?= $post['User']['first_name']; ?></br>
                    <?= $post['User']['username']; ?>
                  </div>
                </div>
                 <div class="post-content">
                  <div class="post-text">
                   <?=  nl2br(h($post['Post']['post'])); ?>
                  </div>
                    <?php if ($post['Post']['image_name'] != null) :?>
                     <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img'] );?>
                    <?php endif; ?>
                 </div>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Retweet</button>
            </form>
          </div>
      </div>
  </div>
</div>
<!--Edit Model dialog-->
<div class="modal fade" id=<?="editNormalModal".$post['Post']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="demoModalTitle">EDIT</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <form action="/posts/edit" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
          <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
            <input type='hidden' name="data[Post][id]" value="<?= $post['Post']['id']?>">
            <textarea name="data[Post][post]" rows="3" maxlength="140" cols="30" id="PostPost"><?= $post['Post']['post']?></textarea>
            <div class="input file">
              <input type="file" name="data[Post][image_name]" id="PostImageName">
            </div>
            <div class="modal-post">
              <div class="post-items" style="color: #8C55AA;">
                <?= $this->Html->image($post['User']['image_name'],['class' => 'share-img']);?>
                <div class="post-item" style="font-weight: bold;">
                  <?= $post['User']['first_name']; ?></br>
                  <?= '@'.$post['User']['username']; ?>
                </div>
              </div>
               <div class="post-content">
                <div class="post-text">
                 <?= nl2br(h($post['Post']['post'])); ?>
                </div>
                  <?php if ($post['Post']['image_name'] != null) :?>
                   <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img'] );?>
                  <?php endif; ?>
               </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">EDIT</button>
          </form>
        </div>
    </div>
</div>
</div>

<div class="main">
  <div class="main-title">
    <a href="#" onClick="history.back(); return false;" style="color:white;"><i class="fas fa-arrow-left"></i></a>
    </div>

    <ul>
        <li>
          <div class="post-items">
            <?= $this->Html->image($post['User']['image_name'],['class' => 'profile-img']); ?>
            <div class="post-item">
              <?= $post['User']['first_name'];?>
              <?= $this->Html->link( '@'.$post['User']['username'],'/users/profile/'.$post['User']['id']); ?>
            </div>
          </div>
          <div class="post-content">
            <div class="post-text">
              <?=  nl2br(h($post['Post']['post'])); ?>
            </div>
              <?php if ($post['Post']['image_name'] != null):?>
               <?= $this->Html->image($post['Post']['image_name'],['class' => 'post-img']);?>
              <?php endif; ?>
          </div>
          <hr  style="margin:10px 0 5px 0; border-width: 1px;"/>
          <div class="post-btn">

          <a href="#" data-toggle="modal" data-target=<?="#demoNormalModal".$post['Post']['id']?>><i class="fas fa-retweet"></i></a>
          <?php if ($post['Like']) :?>
            <div class="like-btn">
             <a href="#" class="unlike" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
          <span><?= $post['Post']['like_count']?></span>
          </div>
          <?php else: ?>
            <div class="like-btn">
             <a href="#" data-post-id="<?= $post['Post']['id']?>"><i class="far fa-heart"></i></a>
             <span><?= $post['Post']['like_count']?></span>
            </div>
          <?php endif; ?>

          <?php if ($users['User']['id'] == $post['Post']['user_id']) :?>
          <a href="#" data-toggle="modal" data-target=<?="#editNormalModal".$post['Post']['id']?>><i class="fas fa-edit"></i></a>
          <?php endif; ?>

          <?php if ($users['User']['id'] == $post['Post']['user_id']) :?>
            <?= $this->Form->postLink('<i class="fas fa-trash-alt"></i>', ['action'=>'delete', $post['Post']['id']],
            ['escape' => false]
          );?>
          <?php endif; ?>
          </div>
          <hr  style="margin:0 0 0 0;"/>
        </li>
     </ul>
     <ul>
     <?php foreach ($comments as $comment): ?>
  <div class="modal fade" id=<?="editNormalModal".$comment['Comment']['id']?> tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="demoModalTitle">EDIT</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <div class="modal-body">
                     <form action="/comments/edit" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                     <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                       <input type='hidden' name="data[Comment][id]" value="<?= $comment['Comment']['id']?>">
                       <textarea name="data[Comment][comment]" rows="3" maxlength="140" cols="30" id="PostPost"><?= $comment['Comment']['comment']?></textarea>
                       <div class="input file">
                         <input type="file" name="data[Comment][image_name]" id="PostImageName">
                       </div>
                       <input type="hidden" name="data[Comment][post_id]" value="<?= $comment['Comment']['post_id'] ?>">
                       <div class="modal-post">
                         <div class="post-items">
                           <?= $this->Html->image($comment['CommentUser']['image_name'],['class' => 'share-img']);?>
                           <div class="post-item">
                             <?= $comment['CommentUser']['first_name']; ?><br />
                             <?= "@".$comment['CommentUser']['username']; ?>
                           </div>
                         </div>
                          <div class="post-content">
                           <div class="post-text">
                            <?= nl2br(h($comment['Comment']['comment'])); ?>
                           </div>
                           <?php if ($comment['Comment']['image_name'] != null): ?>
                            <?= $this->Html->image($comment['Comment']['image_name'],['class' => 'post-img'] );?>
                          <?php endif ;?>
                          </div>
                       </div>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         <button type="submit" class="btn btn-primary">EDIT</button>
                     </form>
                   </div>
               </div>
           </div>
         </div>

     <li>
       <div class="post-items">
         <?= $this->Html->image($comment['CommentUser']['image_name'],['class' => 'profile-img']); ?>
         <div class="post-item">
           <?= $comment['CommentUser']['first_name'];?>
           <?= $this->Html->link( '@'.$comment['CommentUser']['username'],'/users/profile/'.$comment['CommentUser']['id']); ?>
         </div>
         <?= date('M j(D) H:i', strtotime($comment['Comment']['created']))?>
       <?php if ($comment['Comment']['created'] != $comment['Comment']['modified']):?>
        <i class="fas fa-pen"></i>
       <?php endif; ?>
       </div>
       <div class="post-content">

         <div class="post-text">
           <?= h($comment['Comment']['comment']); ?>
           <?php if ($comment['Comment']['image_name'] != null): ?>
            <?= $this->Html->image($comment['Comment']['image_name'],['class' => 'post-img'] );?>
          <?php endif ;?>
         </div>
       </div>
       <div class="post-btn">
         <?php if ($users['User']['id'] == $comment['Comment']['user_id']) :?>
         <a href="#" data-toggle="modal" data-target=<?="#editNormalModal".$comment['Comment']['id']?>><i class="fas fa-edit"></i></a>
         <?php endif; ?>

         <?php if ($users['User']['id'] == $comment['Comment']['user_id']) :?>
           <?= $this->Form->postLink('<i class="fas fa-trash-alt"></i>', [
             'controller'=>'comments',
            'action'=>'delete', $comment['Comment']['id']],
           ['escape' => false]
         );?>
       <?php endif; ?>
       </div>
     </li>
     <hr />
     <?php endforeach; ?>
     </ul>

     <div class="post-items">
       <?= $this->Html->image($users['User']['image_name'],['class' => 'profile-img']); ?>
       <div class="post-item">
         <form action="/comments/add" novalidate="novalidate" id="PostAddForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">
          <textarea name="data[Comment][comment]" class="textarea" placeholder="What's happening?"rows="3" cols="30" id="CommentComment" required="required"></textarea>
          <input type="hidden" name="data[Comment][post_id]" value="<?= $post['Post']['id']?>" id="CommentPostId">
          <div class="input file">
            <input type="file" name="data[Comment][image_name]" id="PostImageName">
          </div>
          <div class="comment-content" style="padding-left: 350px;">
            <button type="submit" class="btn btn-primary" style="width: 110px;">COMMENT</button>
          </form>
       </div>
      </div>

     <!-- <?php
     echo $this->Form->input('Comment.comment', array('rows'=>3));
     echo $this->Form->input('Comment.post_id', array('type'=>'hidden', 'value'=>$post['Post']['id']));?> -->

     </div>

   </div>

   <div class="side-right">
     <div class="searchform"　>
       <form  name="myForm" action="/posts/search" method="get" onsubmit="return validateForm()">
         <input type="text" name="search" placeholder="Search"/>
         <button type="submit"><i class="fa fa-search" aria-hidden="true"></i>
         </button>
       </form>
     </div>
   </div>

<script>
$(function() {
    $('.like-btn > a').click(function(e) {
        // if (confirm('sure?')) {
         var $this = $(this);
           if($this.hasClass("unlike")){
         $.ajax({
              url: "/Likes/like",
              type: "POST",
              data: { name : $(this).data('post-id') },
              success : function(response){
                  //通信成功時の処理
                    $this.removeClass('unlike');
                    $this.next().html(response);
              },
              error: function(){
                  //通信失敗時の処理
                  alert('Something went wrong');
              }
          });
        } else{
          $.ajax({
               url: "/Likes/like",
               type: "POST",
               data: { name : $(this).data('post-id') },
               success : function(response){
                   //通信成功時の処理
                     $this.addClass('unlike');
                     $this.next().html(response);
               },
               error: function(){
                   //通信失敗時の処理
                   alert('Something went wrong');
               }
           });
          }
        return false;
    });
});
// $(function() {
//     $('a.like').click(function(e) {
//         if (confirm('sure?')) {
//          var $this = $(this);
//          $.ajax({
//               url: "/Likes/Like",
//               type: "POST",
//               data: { name : $(this).data('post-id') },
//               success : function(response){
//                   //通信成功時の処理
//                   alert(response);
//                   $this.text(response);
//               },
//               error: function(){
//                   //通信失敗時の処理
//                   alert('Something went wrong');
//               }
//           });
//         }
//         return false;
//     });
// });

function validateForm() {
  var value = document.forms["myForm"]["search"].value;
  var reg = new RegExp(/^\s+$/);
  if (value == "") {
    alert("Try searching username or keywords");
    return false;
  }
  var regex = new RegExp('[¥¥s]');
  if (reg.test(value)){
    alert("cant make empty! Try searching username or keywords");
    return false;
  }
}

</script>
