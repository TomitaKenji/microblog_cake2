<?php

class Post extends AppModel {
    public $belongsTo = 'User';

    public $hasMany = ["Comment","Like"];

    public $actsAs = array('Containable');

    public $validate = array(
          'post' => array(
              'empty'=> array(
                'rule' => array('notBlank'),
                'message' => 'Please, enter something',
               ),
              'length' => array(
                'rule' => array('maxLength', 140),
                'message' => 'Over 140 characters',
               ),
          ),
          'image_name' => array(
            'extension' => array(
               'rule' => array( 'extension', array(
               'jpg', 'jpeg', 'png', 'gif')  // 拡張子を配列で定義
               ),
               'message' => array( 'Only jpg, jpeg, png, gif')
             ),
             'size' => array(
               'rule' => array('fileSize', '<=', '1MB'),
               'message' => 'Over 1MB'
             )
           )
        );

}
