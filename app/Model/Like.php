<?php

App::uses('AppModel','Model');

class Like extends AppModel
{
  public $belongsTo = [
    'Post' => [
      'className' => 'Post',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => true
    ],
  ];

}
