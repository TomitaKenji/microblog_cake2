<?php

App::uses('AppModel','Model');

class Follower extends AppModel
{
  public $belongsTo = "User";
}
