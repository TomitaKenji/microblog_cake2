<?php
App::uses('AppModel','Model');

class User extends AppModel {

  public $hasMany = array(
       'Post' => array(
           'className' => 'Post'
       )
       ,
       'Follower' => array(
         'foreignKey' => 'followed_id'
       )
   );

   public $actsAs = array('Containable');

   public function isHalfLetter($data) {
    $str = current($data);
    return preg_match('/^[\x21-\x7E]*$/', $str);
}

    public $validate = array(
            'image_name' => array(

            'extension' => array(
                'rule' => array( 'extension', array(
                    'jpg', 'jpeg', 'png', 'gif')  // 拡張子を配列で定義
                ),
                'message' => array( 'Only jpg,jpeg,png,gif')
            ),
         ),
        'username' => array(
            'length' => array(
                'rule' => array('minLength', 5),
                'message' => ' using more than 5 letters and number',
            ),
            'alphanum' => array(
                'rule' => 'alphanumeric',
                'message' => 'Please, enter letters or numbers',
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Already exist your username',
            ),
        ),
        'first_name' => array(
            'length' => array(
                'rule' => array('between', 2, 15),
                'message' => 'Between 2 to 15 letters',
            ),
            'alphanum' => array(
                'rule' => '/^[a-zA-Z]$/i',
                'message' => 'Sorry, only can accept letters',
            ),
        ),
        'last_name' => array(
            'length' => array(
                'rule' => array('between', 2, 15),
                'message' => 'Between 2 to 15 letters',
            ),
            'alphanum' => array(
                'rule' => '/^[a-zA-Z]$/i',
                'message' => 'Sorry, only can accept letters',
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Invaild Email',
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Already exist your email',
            ),
        ),
        'password' => array(
            'length' => array(
                'rule' => array('between', 8, 25),
                'message' => 'Between 8 to  25 of words',
        ),
      ),
        'password_confirm' => array(
          'password_confirm' => array(
            'rule' => array('passwordCheck'),
            'message' => 'do not match password and confirm password',
        ),
      ),
    );
  // );

  public function passwordCheck()
  {
        if($this->data['User']['password'] === $this->data['User']['password_confirm']){
            return true;
        } else {
            return false;
        }
    }
    //--------------------------------------------------------------------------
    // ヘルパー関数

    // パスワードと確認入力が一致するかチェックする
    public function password_match($field, $password)
    {
        return ($field['password_confirm'] === $this->data[$this->name][$password]);
    }

    // 本登録用のリンクに含めるハッシュを生成する
    public function getActivationHash()
    {
        if (!isset($this->id)) {
            return false;
        } else {
            return Security::hash($this->field('modified'), 'md5', true);
        }
    }

    //--------------------------------------------------------------------------
    // コールバック関数

    // データが保存される前に実行される
    public function beforeSave($options = array()) {
        // 平文パスワードをハッシュ化
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
}
